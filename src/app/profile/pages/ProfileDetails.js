import React from 'react';
import Details from "../components/Details";
import {connect} from "react-redux";
import {getProfileDetails, handleLike} from "../actions/profileActions";
import {bindActionCreators} from "redux";

class ProfileDetails extends React.Component{
  componentDidMount() {
    this.props.getProfileDetails(this.props.match.params.id);
  }

  handleLike(){
    const data = {
      userProfileId: this.props.match.params.id,
      likedBy: 'anonymous user'
    };
    this.props.handleLike(data);
  }

  render() {
    const {name, gender, description} = this.props.profileDetails;
    return (
      <div>
        <h3>User Profile</h3>
        <Details name={name} gender={gender} desc={description}  />
        <button onClick={this.handleLike.bind(this)}>Like</button>
      </div>
    );
  }
}

const mapStateToProps = state => state.profile;

const mapDispatchToProps = dispatch => bindActionCreators(
  {getProfileDetails, handleLike}, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(ProfileDetails);