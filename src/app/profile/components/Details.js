import React from 'react';

const Details = ({name, gender, desc}) => {
  return (
    <div>
      <div>
        <label>User Name: </label>
        <span>{name}</span>
      </div>
      <div>
        <label>Gender: </label>
        <span>{gender}</span>
      </div>
      <div>
        <label>Description: </label>
        <span>{desc}</span>
      </div>
    </div>
  );
};

export default Details;