export const getProfileDetails = (id) => (dispatch) => {
  fetch(`http://localhost:8080/api/user-profiles/${id}`)
    .then(res => res.json())
    .then((res) => {
      dispatch({
        type: 'GET_PROFILE_DETAILS',
        profileDetails: res
      });
    })
};

export const handleLike = (data) => (dispatch) => {
  fetch('http://localhost:8080/api/like', {
    method: 'POST',
    headers: {
      'Content-Type':  'application/json'
    },
    body: JSON.stringify(data)
  }).then(res => {
    dispatch({
      type: 'LIKE_CLICK'
    });
  })
};